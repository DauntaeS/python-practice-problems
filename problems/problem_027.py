# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None
    i = values[0]
    for cart in values:
        if cart > i:
            i = cart
            return i
