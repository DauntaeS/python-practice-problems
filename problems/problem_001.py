# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# return minimum of two values
# if values are equal return either value

def minimum_value(value1, value2):
    if value1 == value2:
        return value2

print(minimum_value(5, 5))
