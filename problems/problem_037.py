# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

# Step 1 turn Number into a string
# Step 2 make String from number as long as Length
# Step 3 add pad to the left of the result

def pad_left(number, length, pad):
    i = str(number)
    while len(i) < length:
        i = pad + i
        return i

pad_left(1, 5, "5")
