# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

# create a list that holds numbers and returns the average
# if list is empty return none


# if the list of values is 0 return None
# take a list of numbers and returns average of all numbers

def calculate_average(*values):
    i = 0
    if len(values) == 0:
        return None
    if len(values) >= 1:
        i = values
        add = sum(i)
        divide = add / len(values)
        return int(divide)

calculate_average()
calculate_average(10, 10, 50, 10, 10)
