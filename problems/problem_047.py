# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    has_lowerCase = False
    has_upperCase = False
    has_digit = False
    has_specialChar = False
    for secure_pword in password:
        if secure_pword.isalpha():
            if secure_pword.islower():
                has_lowerCase = True
            else:
                secure_pword.isupper()
                has_upperCase = True
        elif secure_pword.isdigit():
            has_digit = True
        elif secure_pword == "!" or secure_pword == "@" or secure_pword == "*":
            has_specialChar = True
            print("That's a strong password, good choice!")
        else:
            len(secure_pword) < 6 and len(secure_pword) > 12
            return "Please try another password"


















check_password(["Password1"])
