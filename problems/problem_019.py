# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

# x = 10
# y = 10
# rect_x = 5
# rect_y = 6
# rect_width = 8
# rect_height = 7

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    combine = rect_x + rect_width
    combine2 = rect_y + rect_height
    if x >= rect_x and y >= rect_y and x <= combine and y <= combine2:
        return True
    else:
        return False

is_inside_bounds(10, 10, 5, 6, 8, 7)
is_inside_bounds(5, 5, 20, 30, 88, 100)
