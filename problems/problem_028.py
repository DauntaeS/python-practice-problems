# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

# "string" & returns a "string" minus all copys

def remove_duplicate_letters(*s):
    i = ""
    for copy in s:
       if copy not in i:
           i = i + copy
    return i

remove_duplicate_letters("a", "b", "c", "a")
