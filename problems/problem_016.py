# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

#

def is_inside_bounds(x, y):
    if x and y > 0 and x and y < 10:
        return True
    else:
        return False

is_inside_bounds(5, 5)
is_inside_bounds(12, 20)
is_inside_bounds(0, -1)
