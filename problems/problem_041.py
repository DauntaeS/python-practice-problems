# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.



# append to result at end

def add_csv_lines(csv_lines):
    # i = empty list
    i = []
    for str in csv_lines:
        # for indexes in csv line split the string by commas and add that to part_1
        part_1 = str.split(",")
        # empty variable to catch the indexes as the cpu iterates over them
        sum = 0
        for total in part_1:
    #         # push total into the add variable and store as an interger
            add = int(total)
            sum += add
            i.append(sum)
    return i
