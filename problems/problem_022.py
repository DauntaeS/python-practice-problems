# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.

#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"

#   * If it is a workday, the list needs to contain "laptop"

#   * If it is not a workday, the list needs to contain
#     "surfboard"

get_ready = ["umbrella", "laptop", "surfboard"]

def gear_for_day(is_workday, is_sunny):
    if ("work" in is_workday and "raining" in is_sunny):
        return get_ready[0]
    if ("work" in is_workday):
        return get_ready[1]
    if ("day off" in is_workday):
        return get_ready[2]
    else:
        return False

# gear_for_day("work", "raining")
# gear_for_day("day off", "sunny")
gear_for_day("idk", "clear skys")
